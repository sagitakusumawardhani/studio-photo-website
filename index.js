const express = require('express')
const { controller, view } = require('./router')
const db = require('./db/db.js')
const path = require('path')
const { orderPage } = require('./controller')
const app = express()

app.use(express.json()) // middleware for accept json type
app.use(express.urlencoded({extended: false})) //middleware for urlencoded
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// controller
app.use('/api/v1/studio', controller)

// view
app.use('/', view)

app.listen(3000, () => {
    console.log('listening on port 3000')
})

