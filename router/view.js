const view = require('express').Router();

const {landingPage, cataloguePage, packagePage} = require('../controller');
const orderPage = require('../controller/orderPage');
view.get('/', landingPage)
view.get('/catalogue', cataloguePage)
view.get('/package', packagePage)
view.get('/order', orderPage)

module.exports = view