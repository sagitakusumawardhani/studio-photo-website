const controller = require('express').Router();

const {postStudioPackage, updateStudioPackagePrice, deleteStudioPackage, postOrder} = require('../controller');


controller.post('/create-package', postStudioPackage)
controller.delete('/delete-package', deleteStudioPackage)
controller.put('/update-package', updateStudioPackagePrice)
controller.post('/create-order', postOrder)

module.exports = controller