/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function(knex) {
    return knex.schema.createTable('tb_order', (table) =>{
            table.increments('order_id').primary();
            table.string('customer_name', 255);
            table.string('customer_email', 255);
            table.string('customer_phone_number', 255);
            table.string('package_order', 255);
            table.string('color_order', 255);
            table.string('time_order', 255);
        }).createTable('tb_package', (table) =>{
            table.increments('package_id').primary();
            table.string('package_name', 255);
            table.integer('package_price');
        })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return Promise.all ([
        knex.schema.dropTableIfExists("tb_order"),
        knex.schema.dropTableIfExists("tb_package")
    ])
};
