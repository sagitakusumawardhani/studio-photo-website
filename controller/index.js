
const postStudioPackage = require('./postStudioPackage')
const deleteStudioPackage = require('./deleteStudioPackage')
const updateStudioPackagePrice = require('./updateStudioPackagePrice')
const postOrder = require('./postOrder')
const landingPage = require('./landingPage')
const cataloguePage = require('./cataloguePage')
const packagePage = require('./packagePage')
const orderPage = require('./orderPage')


module.exports = {
    postStudioPackage,
    deleteStudioPackage,
    updateStudioPackagePrice,
    postOrder,
    landingPage,
    cataloguePage,
    packagePage,
    orderPage
}