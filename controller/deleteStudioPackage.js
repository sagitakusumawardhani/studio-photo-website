const db = require("../db/db")


module.exports = async (req, res) => {
    //const packageName = req.params.package_name
    const packageName = req.query.package_name
    
    console.log(packageName)
    try {
        await db('tb_package').where('package_name', packageName).delete()

        return res.status(201).json({message : 'delete package success'})
    } catch (error) {
        console.log(error)
        return res.status(400).json({message: 'internal server error'})
    }
}
