const db = require("../db/db")


module.exports = async (req, res) => {
    const payload = req.body
    console.log(payload)
    try {
        await db('tb_order').insert({
            customer_name: payload.customer_name,
            customer_email: payload.customer_email,
            customer_phone_number: payload.customer_phone_number,
            package_order: payload.package_order,
            color_order: payload.color_order,
            time_order: payload.time_order
        })

        return res.status(201).json({message : 'order success'})
    } catch (error) {
        console.log(error)
        return res.status(400).json({message: 'order failed'})
    }
}
