const db = require("../db/db")


module.exports = async (req, res) => {
    const payload = req.body
    console.log(payload)
    try {
        await db('tb_package').insert({
            package_name: payload.package_name,
            package_price: payload.package_price,
        })

        return res.status(201).json({message : 'add package success'})
    } catch (error) {
        console.log(error)
        return res.status(400).json({message: 'internal server error'})
    }
}
